package com.example.mytest;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.*;

import java.util.List;
import java.util.Map;
import java.util.Set;

@SpringBootTest
public class RedisTest {

    @Autowired
    StringRedisTemplate redisTemplate;

    /**
     * String操作
     */
    @Test
    void stringTest() {
        //得到操作String的对象
        ValueOperations<String, String> ops = redisTemplate.opsForValue();
        // 存入redis
        ops.set("张三", "25");
        ops.set("李四", "25");
        // 修改
        ops.set("张三", "30");
        //从redis取出
        String s = ops.get("张三");
        System.out.println(s);
        //从redis删除
        Boolean delete = redisTemplate.delete("张三");
        System.out.println("删除结果:"+delete);
    }

    /**
     * List操作
     */
    @Test
    void listTest(){
        //得到操作List的对象
        ListOperations<String, String> ops = redisTemplate.opsForList();
        //保存list的key
        String key = "listKey";
        //从右边推入list
        ops.rightPush(key,"20");
        ops.rightPush(key,"20");
        //从左边推入list
        ops.leftPush(key,"30");
        //从redis删除1个值
        ops.remove(key,1,"20");
        //取出list中所有的值
        List<String> range = ops.range(key, 0, -1);
        //打印输出
        System.out.println(range);
    }

    /**
     * Set操作
     */
    @Test
    void setTest(){
        //得到操作Set的对象
        SetOperations<String, String> ops = redisTemplate.opsForSet();
        //保存数据的key
        String key = "setKey";
        //往Set里面添加数据
        ops.add(key,"张三");
        ops.add(key,"李四");
        ops.add(key,"王五");
        //删除
        ops.remove(key,"张三");
        //查出Set里面所有数据,输出
        Set<String> members = ops.members(key);
        System.out.println(members);
    }

    /**
     * Hash操作
     */
    @Test
    void hashTest(){
        //得到操作Hash的对象
        HashOperations<String, Object, Object> ops = redisTemplate.opsForHash();
        //保存数据的key
        String key = "hashKey";
        //往Hash里面添加数据
        ops.put(key,"张三","20");
        ops.put(key,"李四","30");
        ops.put(key,"王五","40");
        //删除
        ops.delete(key,"张三");
        //查出Hash里面所有数据,输出
        Map<Object, Object> entries = ops.entries(key);
        System.out.println(entries);
    }

    /**
     * sort set操作
     */
    @Test
    void sortSetTest(){
        //得到操作sort set的对象
        ZSetOperations<String, String> ops = redisTemplate.opsForZSet();
        //保存list的key
        String key = "zSetKey";
        //添加数据
        ops.add(key, "张三", 80);
        ops.add(key, "李四", 100);
        ops.add(key, "王五", 60);
        //删除
        ops.remove(key,"李四");
        //查出里面所有数据,输出
        Set<String> range = ops.range(key, 0, -1);
        System.out.println(range);
    }


}
