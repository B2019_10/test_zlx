package com.example.mytest;

import com.example.mytest.entity.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * 数据库操作单元测试
 */
@SpringBootTest
public class JdbcTest {

    @Autowired
    JdbcTemplate jdbcTemplate;

    /**
     * 新增
     */
    @Test
    void insertTest(){
        //执行新增,返回新增条数
        int insert = jdbcTemplate.update("insert into user(name,age) values('王五',25)");
        //输出新增条数
        System.out.println(insert);
    }

    /**
     * 删除
     */
    @Test
    void deleteTest(){
        //删除记录,返回删除条数
        int delete = jdbcTemplate.update("delete from user where name = '王五'");
        //输出删除条数
        System.out.println(delete);
    }

    /**
     * 修改
     */
    @Test
    void updateTest(){
        //删除记录,返回删除条数
        int update = jdbcTemplate.update("update user set age = 30 where name = '王五'");
        //输出删除条数
        System.out.println(update);
    }

    /**
     * 查询
     */
    @Test
    void selectTest(){
        //查询返回结果集
        List<User> userList = jdbcTemplate.query("select * from user", new RowMapper<User>() {
            //封装结果集
            @Override
            public User mapRow(ResultSet resultSet, int i) throws SQLException {
                //从结果里面取出id
                int id = resultSet.getInt("id");
                //从结果里面取出name
                String name = resultSet.getString("name");
                //从结果里面取出age
                int age = resultSet.getInt("age");
                //构造一个User对象返回
                return new User(id, name, age);
            }
        });
        //输出结果集
        System.out.println(userList);
        //循环打印输出
        /*for (User user : userList) {
            System.out.println(user);
        }*/
    }
}
