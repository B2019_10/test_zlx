CREATE DATABASE `my_test` CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
use `my_test`;
create table user
(
	id int auto_increment comment '主键id',
	name nvarchar(20) null comment '姓名',
	age int null comment '年龄',
	constraint user_pk
		primary key (id)
)
comment '用户表';
insert into user(name,age) values('王五',25);
commit;

