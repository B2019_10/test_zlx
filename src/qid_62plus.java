
public class qid_62plus {
    public static void main(String[] args) {
        int n = 4, m = 5;
        Solution0 solution0 = new Solution0();
        System.out.println(solution0.lastRemaining(n, m));
    }
}
//！！！！！！！！！公式法
/**
 *
 */
class Solution0 {
    public int lastRemaining(int n, int m) {
        int ans = 0;
        // 最后一轮剩下2个人，所以从2开始反推
        for (int i = 2; i <= n; i++) {
            ans = (ans + m) % i;
        }
        return ans;
    }
}

//https://blog.csdn.net/u011500062/article/details/72855826